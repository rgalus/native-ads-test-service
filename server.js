const env = require('dotenv-safe').config()
const path = require('path')
const koa = require('koa')
const serve = require('koa-static')
const winston = require('winston')

const app = new koa()
const public = path.join(__dirname, 'dist')

const PORT = process.env.PORT || 3000

app.use(serve(public))

app.listen(PORT, () => {
    winston.log('info', `Listening on port ${PORT}`)
})
