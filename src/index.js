import React, { PureComponent } from 'react'
import ReactDOM from 'react-dom'

import { Bling as Gpt } from 'react-gpt'

Gpt.configure({
    seedFileUrl: 'https://www.googletagservices.com/tag/js/gpt.js',
})

Gpt.enableSingleRequest()

const styles = {
    topHomepageLeaderboard: {
        textAlign: 'center',
        marginBottom: '32px',
    },
    btmHomepageLeaderboard: {
        textAlign: 'center',
        marginBottom: '32px',
    },
    topTvLeaderboard: {
        textAlign: 'center',
        paddingTop: '16px',
        paddingBottom: '16px',
        background: '#353537',
    },
    videosTvLeaderboard: {
        textAlign: 'center',
        marginBottom: '16px',
    },
    btmTvLeaderboard: {
        textAlign: 'center',
        marginTop: '16px',
        marginBottom: '48px',
    },
    flex: {
        flex: '0 0 100%',
    },
    popularGoalsNative: {
        textAlign: 'center',
        marginBottom: '20px',
    },
}

const sizeMappings = {
    leaderboards: [
        { viewport: [0, 0],   slot: [240, 133] },
        { viewport: [400, 0],   slot: [336, 280] },
        { viewport: [600, 0],   slot: [320, 50] },
        { viewport: [1200, 1],  slot: [728, 90] },
    ],
    tvLeaderboards: [
        { viewport: [0, 0],   slot: [320, 50] },
        { viewport: [500, 0],   slot: [468, 60] },
        { viewport: [980, 0],   slot: [728, 90] },
        { viewport: [1280, 1],  slot: [970, 90] },
    ],
    btmTvLeaderboards: [
        { viewport: [0, 0],   slot: [300, 250] },
        { viewport: [340, 0],   slot: [336, 280] },
        { viewport: [1000, 1],  slot: [970, 250] },
    ],
    popularGoals: [
        { viewport: [0, 0],   slot: 'fluid' },
    ],
}

const configs = {
    topHomepageLeaderboard: {
        adUnitPath: '/37686362/ADX_Display/Mycujoo.tv_Display_TopLeaderboard',
        id: 'top-home-leaderboard',
        collapseEmptyDiv: true,
        sizeMapping: sizeMappings.leaderboards,
        style: styles.topHomepageLeaderboard,
    },
    btmHomepageLeaderboard: {
        adUnitPath: '/37686362/ADX_Display/Mycujoo_Display_Bottom_Leaderboard',
        id: 'btm-home-leaderboard',
        collapseEmptyDiv: true,
        sizeMapping: sizeMappings.leaderboards,
        style: styles.btmHomepageLeaderboard,
    },
    topTvPageLeaderboard: {
        adUnitPath: '/37686362/ADX_Display/AdX_Display_TVPage_Top',
        id: 'top-tv-leaderboard',
        collapseEmptyDiv: true,
        sizeMapping: sizeMappings.tvLeaderboards,
        style: styles.topTvLeaderboard,
    },
    videosTvPageLeaderboard: {
        adUnitPath: '/37686362/ADX_Display/AdX_Display_TVPage_Bottom',
        id: 'videos-tv-leaderboard',
        collapseEmptyDiv: true,
        sizeMapping: sizeMappings.tvLeaderboards,
        style: styles.videosTvLeaderboard,
        containerStyle: styles.flex,
    },
    btmTvPageLeaderboard: {
        adUnitPath: '/37686362/ADX_Display/AdX_Display_TVPage_Bottom',
        id: 'btm-tv-leaderboard',
        collapseEmptyDiv: true,
        sizeMapping: sizeMappings.btmTvLeaderboards,
        style: styles.btmTvLeaderboard,
    },
    popularGoalsNative: {
        adUnitPath: '/37686362/ADX_Display/Mycujoo_Display_NativeAd_PopularGoals',
        id: 'popular-goals-native',
        collapseEmptyDiv: true,
        sizeMapping: sizeMappings.popularGoals,
        style: styles.popularGoalsNative,
    },
    test: {
        adUnitPath: '/37686362/ADX_Display/Mycujoo_Display_NativeAd',
        id: 'test',
        collapseEmptyDiv: true,
        sizeMapping: sizeMappings.popularGoals,
        style: styles.popularGoalsNative,
    }
}

class AdGptContainer extends PureComponent {
    render () {
        const { name, targeting } = this.props
        const { id, containerStyle, ...config } = configs[name]

        if (config === undefined) {
            return <div />
        }

        return (
            <Gpt {...config} targeting={targeting} />
        )
    }
}

for (let adType in configs) {
    if (configs.hasOwnProperty(adType)) {
        ReactDOM.render(
            <AdGptContainer name={adType} targeting={{}} />,
            document.getElementById(adType)
        );
    }
}
